#!/bin/sh
#
# checks for any problems with my zfs pool 
# emails me if theres a problem

# email content
alert=$(cat << EOF
To: tomasbulk@gmail.com
From: tomasbulk@gmail.com
Subject: TITAN: ZFS POOL COMPROMISED

$(zpool status)
EOF
)

# check for errors
if [ "$(zpool status | awk '/errors:/')" == "errors: No known data errors" ]; then
	exit 0
else
	printf "$alert" | msmtp -a default tomasbulk@gmail.com
	exit 1
fi
